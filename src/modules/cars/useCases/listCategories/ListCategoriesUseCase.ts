import Category from "../../model/Category";
import { ICategoriesRepository } from "../../repositories/ICategoriesRepository";

class ListCategoriesUseCase {
  constructor(private categoriesRepisitory: ICategoriesRepository) {}
  execute(): Category[] {
    const categories = this.categoriesRepisitory.list();
    return categories;
  }
}

export { ListCategoriesUseCase };
